package ru.kazakov.tm.controller;

import java.util.ArrayDeque;
import java.util.Deque;

public class SystermController extends AbstractController {

    public Deque<String> history = new ArrayDeque<>();

    public int historyLinit = 10;

    public int displayHistory() {
        if (history == null || history.isEmpty()) {
            System.out.println("[COMMANDS ARE NOT FOUND]");
            return -1;
        }
        int index = 1;
        for (final String command : history) {
            System.out.println(index + ") " + command);
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void addCommandToHistory(String param) {
        if (param == null) return;
        history.add(param);
        if (history.size() > historyLinit){
            history.remove();
        }

    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp() {
        System.out.println("[** SYSTEM COMMANDS **]");
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println("history - History of commands.");
        System.out.println();
        System.out.println("[** PROJECT COMMANDS **]");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-list - Display all projects.");
        System.out.println("project-view-by-id - Display project by id.");
        System.out.println("project-view-by-index - Display project by index.");
        System.out.println("project-view-by-name - Display project by name.");
        System.out.println("project-remove-by-id - Delete project by id.");
        System.out.println("project-remove-by-index - Delete project by index.");
        System.out.println("project-remove-by-name - Delete project by name");
        System.out.println("project-update-by-id - Update project by id.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println("project-update-by-name - Update project by name.");
        System.out.println();
        System.out.println("[** TASK COMMANDS **]");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-list - Display all tasks.");
        System.out.println("task-view-by-id - Display task by id.");
        System.out.println("task-view-by-index - Display task by index.");
        System.out.println("task-view-by-name - Display task by name.");
        System.out.println("task-remove-by-id - Delete task by id.");
        System.out.println("task-remove-by-index - Delete task by index.");
        System.out.println("task-remove-by-name - Delete task by name");
        System.out.println("task-update-by-id - Update task by id.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task-update-by-name - Update task by name.");
        System.out.println("task-list-by-project-id - Display task list by project id.");
        System.out.println("task-add-to-project-by-ids - Add task to project by id.");
        System.out.println("task-remove-from-project-by-ids - Remove task from project by ids.");
        System.out.println();
        System.out.println("[** USER COMMANDS **]");
        System.out.println("log-on - Authenticate users.");
        System.out.println("log-off - Log out of the user's system.");
        System.out.println("user-info - Display the current user's information.");
        System.out.println("user-update - Update user information.");
        System.out.println("user-update-password - Update user password.");
        System.out.println("user-create - Create new user.");
        System.out.println("user-clear - Clear all users.");
        System.out.println("user-list - Display all users.");
        System.out.println("user-view-by-id - Display user by id.");
        System.out.println("user-view-by-index - Display user by index.");
        System.out.println("user-view-by-login - Display user by login.");
        System.out.println("user-remove-by-id - Delete user by id.");
        System.out.println("user-remove-by-index - Delete user by index.");
        System.out.println("user-remove-by-login - Delete user by login");
        System.out.println("user-update-by-id - Update user by id.");
        System.out.println("user-update-by-index - Update user by index.");
        System.out.println("user-update-by-login - Update user by login.");

        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.14");
        return 0;
    }

    public int displayAbout() {
        System.out.println("Kazakov Sergey");
        System.out.println("kazakov_si@nlmk.com");
        return 0;
    }

}
