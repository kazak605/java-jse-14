package ru.kazakov.tm.controller;

import ru.kazakov.tm.entity.Task;
import ru.kazakov.tm.service.ProjectTaskService;
import ru.kazakov.tm.service.TaskService;
import ru.kazakov.tm.service.UserService;

import java.util.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService, UserService userService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String description = scanner.nextLine();
        taskService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEAS, ENTER TASK ID:]");
        final Long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findById(id, userId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER TASK NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
            final String description = scanner.nextLine();
            taskService.update(task.getId(), name, description, userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.findByIndex(index, userId);
            if (task == null) {
                System.out.println("[FAIL]");
            } else {
                System.out.println("[PLEASE, ENTER TASK NAME:]");
                final String name = scanner.nextLine();
                System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
                final String description = scanner.nextLine();
                taskService.update(task.getId(), name, description, userId);
                System.out.println("[OK]");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("[PLEASE, ENTER TASK ID:]");
        final long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.removeById(id, userId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.removeByIndex(index, userId);
            if (task == null) {
                System.out.println("[FAIL]");
            } else {
                System.out.println("[OK]");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        final List<Task> tasks = taskService.removeByName(name, userId);
        if (tasks == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("USER_ID: " + task.getUserId());
        System.out.println("[OK]");
    }

    public int viewTaskById() {
        System.out.println("ENTER, TASK ID:");
        final long id = scanner.nextLong();
        final Long userId = userService.currentUser.getId();
        final Task task = taskService.findById(id, userId);
        viewTask(task);
        return 0;
    }

    public int viewTaskByIndex() {
        System.out.println("[PLEASE, ENTER TASK INDEX:]");
        final Scanner scanner = new Scanner(System.in);
        try {
            final int index = Integer.parseInt(scanner.nextLine()) - 1;
            final Long userId = userService.currentUser.getId();
            final Task task = taskService.findByIndex(index, userId);
            viewTask(task);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input type (must be an integer)");
            return -1;
        }
        return 0;
    }

    public int viewTaskByName() {
        System.out.println("[PLEASE, ENTER TASK NAME:]");
        final String name = scanner.nextLine();
        final Long userId = userService.currentUser.getId();
        final List<Task> tasks = taskService.findByName(name, userId);
        viewTasks(tasks);
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        List<Task> taskList;
        if (userService.currentUser == null) {
            taskList = taskService.findAll();
        } else {
            taskList = taskService.findAllByUserId(userService.currentUser.getId());
        }
        viewTasks(taskList);
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("[TASKS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Task task : tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASKS BY PROJECT BY PROJECT ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        final Long userId = userService.currentUser.getId();
        projectTaskService.addTaskToProject(projectId, taskId, userId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskToProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        if (userService.currentUser == null) {
            taskService.clear();
            projectTaskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.currentUser.getId())) {
                taskService.removeById(task.getId(), userService.currentUser.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

}
