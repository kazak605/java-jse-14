package ru.kazakov.tm.repository;

import ru.kazakov.tm.entity.Project;
import ru.kazakov.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    private HashMap<String, List<Task>> tasksAssoc = new HashMap<>();

    public void addToMap(final Task task) {
        List<Task> tasksInMap = tasksAssoc.get(task.getName());
        if (tasksInMap == null) tasksInMap = new ArrayList<>();
        tasksInMap.add(task);
        tasksAssoc.put(task.getName(), tasksInMap);
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description, userId);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description, final Long userId ) {
        Task task = findById(id,userId);
        if (task == null) return null;
        String oldName = task.getName();

        List<Task> taskListOld = findByName(oldName, userId);
        if (taskListOld == null) return null;
        task.setName(name);
        task.setDescription(description);
        if (!oldName.equals(name) && taskListOld.size() > 1) {
            List<Task> taskListNew = new ArrayList<>();
            taskListNew.add(task);
            taskListOld.remove(task);
            tasksAssoc.remove(oldName);
            tasksAssoc.put(oldName, taskListOld);
            tasksAssoc.put(name, taskListNew);
        } else {
            tasksAssoc.remove(oldName);
            tasksAssoc.put(name, taskListOld);
        }
        return task;
    }

    public Task findByIndex(int index, final Long userId) {
        List<Task> result;
        if (userId == null) result = findAll();
        else result = findAllByUserId(userId);
        if (index < 0 || index > result.size() - 1) return null;
        return result.get(index);
    }

    public List<Task> findByName(final String name, final Long userId) {
        if (userId == null) return tasksAssoc.get(name);
        List<Task> result = new ArrayList<>();
        if (tasksAssoc.get(name) == null) return null;
        for (Task task: tasksAssoc.get(name)) {
            if(task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

    public Task findById(final Long id, final Long userId) {
        List<Task> currentListTask;
        if (userId == null) currentListTask = findAll();
        else currentListTask = findAllByUserId(userId);
        for (final Task task : currentListTask) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByIndex(final int index, final Long userId) {
        Task task = findByIndex(index, userId);
        if (task == null) return null;
        tasks.remove(task);
        List<Task> taskFromName = findByName(task.getName(), userId);
        tasksAssoc.remove(task.getName());
        taskFromName.remove(task);
        tasksAssoc.put(task.getName(), taskFromName);
        return task;
    }

    public Task removeById(final Long id, final Long userId) {
        Task task = findById(id, userId);
        if (task == null) return null;
        tasks.remove(task);
        List<Task> taskFromName = findByName(task.getName(), userId);
        tasksAssoc.remove(task.getName());
        taskFromName.remove(task);
        tasksAssoc.put(task.getName(), taskFromName);
        return task;
    }

    public List<Task> removeByName(final String name, final Long userId) {
        List<Task> taskList = findByName(name, userId);
        if (taskList == null || taskList.size() == 0) return null;
        tasks.removeAll(taskList);
        tasksAssoc.remove(name);
        return taskList;
    }

    public void clear() {
        tasks.clear();
        tasksAssoc.clear();
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long IdUser = task.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAll() {
        return tasks;
    }

}
